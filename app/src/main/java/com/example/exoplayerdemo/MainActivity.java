package com.example.exoplayerdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.DownloadManager;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

import okhttp3.CacheControl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


public class MainActivity extends AppCompatActivity implements Player.EventListener{

    PlayerView playerView;
    ImageView settings;
    ImageView fullScreen;
    ImageView fullScreenExit;
    ProgressBar loading;
    EditText forward;
    EditText rewind;
    private static int frwrd = 10;
    private static int rewnd = 10;

    private SimpleExoPlayer simpleExoPlayer;
    private Uri video;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        playerView = findViewById(R.id.player_view);
        fullScreen = findViewById(R.id.fullscreen);
        fullScreenExit = findViewById(R.id.fullscreen_exit);
        loading = findViewById(R.id.loading);
        settings = findViewById(R.id.settings);
        initializeView();
        getVidoeUrl("https://www.youtube.com/watch?v=MdQOXoEMLOs");
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    private void getVidoeUrl(String youtubeUrl) {
        new AsyncTask() {
                @Override
                protected Object doInBackground(Object[] objects) {
                    HashMap<String, String> map = new HashMap<>();
                    try {
                        String youtubeUrl = (String)objects[0];
                        int id_index = youtubeUrl.lastIndexOf('=');
                        if (id_index < 0 || id_index >= youtubeUrl.length() - 1)
                            return null;
                        String video_id = youtubeUrl.substring(id_index + 1);
                        String videoInfoUrl = "https://www.youtube.com/get_video_info?video_id=" + video_id + "&el=embedded&ps=default&eurl=&gl=US&hl=en";
                        Request request = new Request.Builder()
                                .cacheControl(CacheControl.FORCE_NETWORK)
                                .url(videoInfoUrl)
                                .build();
                        OkHttpClient client = new OkHttpClient();
                        final Response response = client.newCall(request).execute();
                        InputStream inputStream = response.body().byteStream();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                        String line;
                        final StringBuilder contentBuilder = new StringBuilder();
                        while ((line = bufferedReader.readLine()) != null) {
                            contentBuilder.append(line);
                        }

                        final String streamKey = "url_encoded_fmt_stream_map";
                        final String content = URLDecoder.decode(contentBuilder.toString(), "UTF-8");
                        for (int i = 0; i + 20 < content.length(); i++) {
                            if (content.substring(i, i + 20).equals("\"url\":\"https://r3---")) {
                                String val = "";
                                int j = i + 7;
                                for (; j < content.length(); j++) {
                                    if (content.charAt(j) == '\"') {
                                        break;
                                    }
                                    val += content.charAt(j);
                                }
                                i = j;
                                val = val.replace("\\u0026", "&");
                                map.put("url", URLDecoder.decode(val, "UTF-8"));
                                break;
                            }
                        }
                        Log.d("SOME_TAG", "VAL: " + map.toString());

                    } catch (Exception e) {
                        Log.d("SOME_TAG", "MSG", e);
                    }
                    return map;
                }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                HashMap<String, String> map = (HashMap) o;
                String url = map.get("url");
                if (url != null) {
                    video = Uri.parse(url);
                }
                initializeView();
            }
        }.execute(youtubeUrl);
    }

    private void initializeView() {
        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        simpleExoPlayer = ExoPlayerFactory.newSimpleInstance(MainActivity.this, new DefaultTrackSelector(new AdaptiveTrackSelection.Factory(bandwidthMeter)));
        DefaultDataSourceFactory factory = new DefaultDataSourceFactory(this, Util.getUserAgent(this, "exoplayerdemo"));
        ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();

        MediaSource mediaSource = new ExtractorMediaSource(video, factory, extractorsFactory, null, null);
        playerView.setPlayer(simpleExoPlayer);
        playerView.setKeepScreenOn(true);
        simpleExoPlayer.prepare(mediaSource);
        simpleExoPlayer.setPlayWhenReady(true);
        simpleExoPlayer.addListener(this);

        fullScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                fullScreen.setVisibility(View.GONE);
                fullScreenExit.setVisibility(View.VISIBLE);
            }
        });

        fullScreenExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                fullScreenExit.setVisibility(View.GONE);
                fullScreen.setVisibility(View.VISIBLE);
            }
        });

        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this);
                LayoutInflater inflater = MainActivity.this.getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.settings_dialog, null);
                dialogBuilder.setView(dialogView);
                final AlertDialog alertDialog = dialogBuilder.create();
                forward = dialogView.findViewById(R.id.forward_time);
                rewind = dialogView.findViewById(R.id.rewind_time);
                Button submit = dialogView.findViewById(R.id.submit_button);
                submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String ahead = forward.getText().toString();
                        String behind = rewind.getText().toString();
                        frwrd = Integer.parseInt(ahead.length() > 0 ? ahead : "10");
                        rewnd = Integer.parseInt(behind.length() > 0 ? behind : "10");
                        playerView.setFastForwardIncrementMs(frwrd * 1000);
                        playerView.setRewindIncrementMs(rewnd * 1000);
                        alertDialog.dismiss();
                    }
                });
                alertDialog.show();
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        simpleExoPlayer.setPlayWhenReady(false);
        simpleExoPlayer.getPlaybackState();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        simpleExoPlayer.setPlayWhenReady(true);
        simpleExoPlayer.getPlaybackState();
    }

    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        if (playbackState == Player.STATE_BUFFERING) {
            loading.setVisibility(View.VISIBLE);
        } else if (playbackState == Player.STATE_READY) {
            loading.setVisibility(View.GONE);
        } else if (playbackState == Player.STATE_ENDED) {
            loading.setVisibility(View.GONE);
        }
    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

    }

    @Override
    public void onPositionDiscontinuity(int reason) {

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onSeekProcessed() {

    }
}